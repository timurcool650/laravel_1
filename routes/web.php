<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\MainController;
use App\Http\Controllers\PriceController;
use App\Http\Controllers\BoxController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/posts', [PostController::class, 'index'])->name('post.index')->middleware('auth');

Route::get('/posts/create', [PostController::class, 'create'])->name('post.create')->middleware('auth');

Route::post('/posts', [PostController::class, 'store'])->name('post.store')->middleware('auth');

Route::get('/posts/{post}', [PostController::class, 'show'])->name('post.show')->middleware('auth');

Route::get('/posts/{post}/edit', [PostController::class, 'edit'])->name('post.edit')->middleware('auth');

Route::patch('/posts/{post}', [PostController::class, 'update'])->name('post.update')->middleware('auth');

Route::delete('/posts/{post}', [PostController::class, 'destroy'])->name('post.destroy')->middleware('auth');
////////////////////////////////////////////////////////////////////////////////////////////////
Route::get('/boxes', [BoxController::class, 'index'])->name('box.index')->middleware('auth');

Route::get('/boxes/create', [BoxController::class, 'create'])->name('box.create')->middleware('auth');

Route::post('/boxes', [BoxController::class, 'store'])->name('box.store')->middleware('auth');

Route::get('/boxes/{box}', [BoxController::class, 'show'])->name('box.show')->middleware('auth');

Route::get('/boxes/{box}/edit', [BoxController::class, 'edit'])->name('box.edit')->middleware('auth');

Route::patch('/boxes/{box}', [BoxController::class, 'update'])->name('box.update')->middleware('auth');

Route::delete('/boxes/{box}', [BoxController::class, 'destroy'])->name('box.destroy')->middleware('auth');
////////////////////////////////////////////////////////////////////////////////////////
Route::get('/prices', [PriceController::class, 'index'])->name('price.index')->middleware('auth');

Route::get('/prices/create', [PriceController::class, 'create'])->name('price.create')->middleware('auth');

Route::post('/prices', [PriceController::class, 'store'])->name('price.store')->middleware('auth');

Route::get('/prices/{price}', [PriceController::class, 'show'])->name('price.show')->middleware('auth');

Route::get('/prices/{price}/edit', [PriceController::class, 'edit'])->name('price.edit')->middleware('auth');

Route::patch('/prices/{price}', [PriceController::class, 'update'])->name('price.update')->middleware('auth');

Route::delete('/prices/{price}', [PriceController::class, 'destroy'])->name('price.destroy')->middleware('auth');
////////////////////////////////////////////////////////////////////////////////////////
Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
