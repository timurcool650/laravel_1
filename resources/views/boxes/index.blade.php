<x-layout.main>
    <div>
        @foreach($boxes as $box)
            <div>
                <a href="{{route('box.show', $box->id)}}">{{$box->id}}. {{$box->title}}</a>
            </div>
        @endforeach
            <a href="{{route('box.create')}}" class="btn btn-success">Create Box</a>
    </div>
</x-layout.main>
