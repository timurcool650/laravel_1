<x-layout.main>
    <form action="{{route('box.store')}}" method="post">
        @csrf
        <div class="mb-3">
            <label for="title" class="form-label">Title</label>
            <input type="text" class="form-control" id="title" name="title">
        </div>
        <div class="mb-3">
            <label for="content" class="form-label">Content</label>
            <textarea class="form-control" id="content" name="content"></textarea>
        </div><div class="mb-3">
            <label for="box" class="form-label">Box</label>
            <input type="text" class="form-control" id="box" name="box">
        </div>
        <div class="mb-3">
            <label for="user_id" class="form-label">User Id</label>
            <textarea class="form-control" id="user_id" name="user_id" readonly>{{$user}}</textarea>
        </div>
        <button type="submit" class="btn btn-primary">Create Box</button>
    </form>
</x-layout.main>
