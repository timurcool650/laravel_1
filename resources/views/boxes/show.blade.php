<x-layout.main>
    <div>Id: {{$box->id}}</div>
    <div>Title: {{$box->title}}</div>
    <div>Content: {{$box->content}}</div>
    <div>Box: {{$box->box}}</div>
    <div>
        <a href="{{route('box.index')}}" class="btn btn-secondary">Back</a>
        @can('update', $box)
        <a href="{{route('box.edit', $box->id)}}" class="btn btn-success">Edit</a>
        @endcan
        <div class="mt-2">
            @can('delete', $box)
            <form action="{{route('box.destroy', $box->id)}}" method="post">
                @csrf
                @method('delete')
                <input type="submit" value="Delete" class="btn btn-danger">
            </form>
            @endcan
        </div>
    </div>
</x-layout.main>
