<x-layout.main>
    <form action="{{route('box.update', $box->id)}}" method="post">
        @csrf
        @method('patch')
        <div class="mb-3">
            <label for="title" class="form-label">Title</label>
            <input type="text" class="form-control" id="title" name="title" value="{{$box->title}}">
        </div>
        <div class="mb-3">
            <label for="content" class="form-label">Content</label>
            <textarea class="form-control" id="content" name="content">{{$box->content}}</textarea>
        </div><div class="mb-3">
            <label for="box" class="form-label">Box</label>
            <input type="text" class="form-control" id="box" name="box" value="{{$box->box}}">
        </div>
        <button type="submit" class="btn btn-primary">Update Box</button>
    </form>
</x-layout.main>
