<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
</head>
<body>
<div class="container">
    <nav class="navbar navbar-expand-lg">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0 fs-5">
                <li class="nav-item">
                    <a class="nav-link" href="{{route('post.index')}}">Posts</a>
                </li><li class="nav-item">
                    <a class="nav-link" href="{{route('box.index')}}">Boxes</a>
                </li> </li><li class="nav-item">
                    <a class="nav-link" href="{{route('price.index')}}">Prices</a>
                </li>
            </ul>
            <a href="{{route('home')}}" class="btn btn-primary">Back to Dashboard</a>
        </div>
    </nav>
    <div>{{ $slot }}</div>
</div>
</body>
</html>
