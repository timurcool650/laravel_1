<x-layout.main>
    <div>
        <div>Id: {{$price->id}}</div>
        <div>Object: {{$price->object}}</div>
        <div>Price: {{$price->price}}</div>
        <a href="{{route('price.index')}}" class="btn btn-secondary">Back</a>
        @can('update', $price)
        <a href="{{route('price.edit', $price->id)}}" class="btn btn-success">Edit</a>
        @endcan
        @can('delete', $price)
        <form action="{{route('price.destroy', $price->id)}}" method="post">
            @csrf
            @method('delete')
            <input type="submit" value="Delete" class="btn btn-danger mt-3">
        </form>
        @endcan
    </div>
</x-layout.main>
