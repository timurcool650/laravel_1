<x-layout.main>
    <form action="{{route('price.update', $price->id)}}" method="post">
        @csrf
        @method('patch')
        <div class="mb-3">
            <label for="object" class="form-label">Object</label>
            <input type="text" class="form-control" id="object" name="object" value="{{$price->object}}">
        </div>
        <div class="mb-3">
            <label for="price" class="form-label">Price</label>
            <input type="text" class="form-control" id="price" name="price" value="{{$price->price}}">
        </div>
        <button type="submit" class="btn btn-primary">Update Object</button>
    </form>
</x-layout.main>
