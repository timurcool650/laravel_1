<x-layout.main>
    <div>
        @foreach($prices as $price)
            <div>
                <a href="{{route('price.show', $price->id)}}">{{$price->id}}. {{$price->object}}</a>
            </div>
        @endforeach
            <a href="{{route('price.create')}}" class="btn btn-success">Add Object</a>
    </div>
</x-layout.main>
