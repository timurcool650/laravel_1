<x-layout.main>
    <form action="{{route('price.store')}}" method="post">
        @csrf
        <div class="mb-3">
            <label for="object" class="form-label">Object</label>
            <input type="text" class="form-control" id="object" name="object">
        </div>
        <div class="mb-3">
            <label for="price" class="form-label">Price</label>
            <input type="text" class="form-control" id="price" name="price"></input>
        </div>
        <div class="mb-3">
            <label for="user_id" class="form-label">User Id</label>
            <textarea class="form-control" id="user_id" name="user_id" readonly>{{$user}}</textarea>
        </div>
        <button type="submit" class="btn btn-primary">Create Object</button>
    </form>
</x-layout.main>
