<x-layout.main>
    <div>
        @foreach($posts as $post)
            <div>
                <a href="{{route('post.show', $post->id)}}">{{$post->id}}. {{$post->title}}</a>
            </div>
        @endforeach
    </div>
    <div>
        <a href="{{route('post.create')}}" class="btn btn-success">Create Post</a>
    </div>
</x-layout.main>
