<x-layout.main>
    <div>
        <div>Id: {{$post->id}}</div>
        <div>Title: {{$post->title}}</div>
        <div>Description: {{$post->Description}}</div>
    </div>
    @can('update', $post)
        <div><a href="{{route('post.edit', $post->id)}}" class="btn btn-success">Edit</a></div>
    @endcan
        <div><a href="{{route('post.index')}}" class="btn btn-secondary mt-3"">Back</a></div>
    @can('delete', $post)
        <form action="{{route('post.destroy', $post->id)}}" method="post">
            @csrf
            @method('delete')
            <input type="submit" class="btn btn-danger mt-3" value="Delete">
        </form>
    @endcan
</x-layout.main>
