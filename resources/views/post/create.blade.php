<x-layout.main>
    <form action="{{route('post.store')}}" method="post">
        @csrf
        <div class="mb-3">
            <label for="title" class="form-label">Title</label>
            <input type="text" class="form-control" id="title" name="title">
        </div>
        <div class="mb-3">
            <label for="Description" class="form-label">Description</label>
            <textarea class="form-control" id="Description" name="Description"></textarea>
        </div>
        <div class="mb-3">
            <label for="user_id" class="form-label">User Id</label>
            <textarea class="form-control" id="user_id" name="user_id" readonly>{{$user}}</textarea>
        </div>
        <button type="submit" class="btn btn-primary">Create</button>
    </form>
</x-layout.main>
