<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Box;
use App\Http\Requests\BoxRequest;
use Illuminate\Support\Facades\Auth;

class BoxController extends Controller
{
    public function index(){
        $boxes = Box::all();
        return view('boxes.index', compact('boxes'));
    }

    public function create(){
        $user = Auth::user()->id;
        return view('boxes.create', compact('user'));
    }

    public function store(BoxRequest $request){
        $validated = $request->validated();
        Box::create($validated);
        return redirect()->route('box.index');
    }

    public function show(Box $box){
        return view('boxes.show', compact('box'));
    }

    public function edit(Box $box){
        $this->authorize('update', $box);
        return view('boxes.edit', compact('box'));
    }

    public function update(BoxRequest $request,Box $box){
        $validated = $request->validated();
        $this->authorize('update', $box);
        $box->update($validated);
        return redirect()->route('box.show', $box->id);
    }

    public function destroy(Box $box){
        $this->authorize('delete', $box);
        $box->delete();
        return redirect()->route('box.index');
    }

}
