<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PostRequest;
use App\Models\Post;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    public function index(){
            $posts = Post::all();
            return view('post.index', compact('posts'));
            }

    public function create(){
        $user = Auth::user()->id;
        return view('post.create', compact('user'));
    }

    public function store(PostRequest $request){
        $validated = $request->validated();
        Post::create($validated);
        return redirect()->route('post.index');
    }

    public function show(Post $post){
        return view('post.show', compact('post'));
    }

    public function edit(Post $post){
        $this->authorize('update', $post);
        return view('post.edit', compact('post'));
    }

    public function update(PostRequest $request, Post $post){
        $this->authorize('update', $post);
        $validated = $request->validated();
        $post->update($validated);
        return redirect()->route('post.show', $post->id);
    }

   public function destroy(Post $post){
        $this->authorize('delete', $post);
        $post->delete();
        return redirect()->route('post.index');
   }
}
