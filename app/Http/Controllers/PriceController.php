<?php

namespace App\Http\Controllers;

use App\Models\Price;
use Illuminate\Http\Request;
use App\Http\Controllers\PriceController;
use App\Http\Requests\PriceRequest;
use Illuminate\Support\Facades\Auth;

class PriceController extends Controller
{
    public function index(){
        $prices = Price::all();
        return view('prices.index', compact('prices'));
    }

    public function create(){
        $user = Auth::user()->id;
        return view('prices.create', compact('user'));
    }

    public function store(PriceRequest $request){
        $validated = $request->validated();
        Price::create($validated);
        return redirect()->route('price.index');
    }

    public function show(Price $price){
        return view('prices.show', compact('price'));
    }

    public function edit(Price $price){
        $this->authorize('update', $price);
        return view('prices.edit', compact('price'));
    }

    public function update(PriceRequest $request,Price $price){
        $validated = $request->validated();
        $price->update($validated);
        return redirect()->route('price.show', $price->id);
    }

    public function destroy(Price $price){
        $this->authorize('delete', $price);
        $price->delete();
        return redirect()->route('price.index');
    }
}
